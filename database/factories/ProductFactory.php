<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Model;
use Faker\Generator as Faker;

$factory->define(App\Product::class, function (Faker $faker) {
    return [
        'name' => $faker->catchPhrase(),
        'description' => $faker->paragraph(3, true),
        'image' => $faker->imageUrl(640, 480),
        'price' => $faker->randomFloat(2, 1000, 5000),
        'category_id' => rand(1, 4)
    ];
});
