<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Purchase extends Model
{
    protected $table = 'purchases';
    protected $primaryKey  = 'id';

    protected $fillable = [
        'total',
    ];

    public $timestamps = false;

    public function products()
    {
        return $this->belongsToMany('App\Product', 'purchase_products', 'purchase_id', 'product_id');
    }
}
